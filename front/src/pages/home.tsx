import { IonContent, IonGrid, IonRow, IonCol, IonList } from '@ionic/react';
import Form from '../components/form/form';


const Home: React.FC = () => {
    return (
    <IonList className='container-component-form'>
        <Form />
    </IonList>
    );
}
export default Home;