import { IonGrid, IonRow, IonCol, IonSearchbar } from '@ionic/react';
import Users from '../components/user/listUser';
import Search from '../components/search/search';
const ListPage: React.FC = () => {
    return (
        <IonGrid className='home-content-row'>
            {/* <div className="home-flex-verticale"> */}
            <IonRow className='home-row-parent'>
             
                    <Users />
                
            </IonRow>
            {/* </div> */}
        </IonGrid>
    );
}
export default ListPage;