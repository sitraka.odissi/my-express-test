import { Redirect, Route } from 'react-router-dom';

import {
  IonApp,
  IonContent,
  IonRouterOutlet,
  setupIonicReact,
  IonPage,
  IonGrid,
  IonRow,
  IonCol,
  IonItem,
  IonList,
  IonText,
  IonBadge
} from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import Home from './pages/home';
import ListPage from './pages/list';
import { NavHeader } from './components/navigation/nav';
/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';
import './sass/app.scss';



setupIonicReact();
const App: React.FC = () => (
  <IonApp>
    <IonContent>
      <IonGrid className='app-wrapper'>
        <IonRow className='app-rows'>
          <IonCol sizeLg='6' sizeMd='4' sizeSm='12' sizeXs='12' className='app-design-content'>
              <IonList className='wrapper-doc-col'>
                <IonList className="doc-content">
                  <IonText><h1>Documentation Front End </h1></IonText>
                  <IonText><h3>Home section</h3></IonText>
                  <IonText >
                    <p>
                      Renseignez les différentes valeurs, du formulaire; notamment: les champs: <br />
                      <IonList>
                        <IonItem>
                          <IonBadge color="warning">Firstname</IonBadge> <IonBadge color="light">type: chaîne de caractères</IonBadge>
                        </IonItem>
                        <IonItem>
                          <IonBadge color="warning">Lastname</IonBadge> <IonBadge color="light">type: chaîne de caractères</IonBadge>
                        </IonItem>
                        <IonItem>
                          <IonBadge color="warning">Email</IonBadge> <IonBadge color="light">type: chaîne de caractères</IonBadge>
                        </IonItem>
                        <IonItem>
                          <IonBadge color="warning">Note</IonBadge> <IonBadge color="light">type: nombre</IonBadge>
                        </IonItem>
                      </IonList>
                      Puis soumettez le formulaire.
                    </p>
                  </IonText>

                  <IonText>
                    <p>
                      L'appliacation fonctionne comme suit: vous pouvez saisir n'importe quelle valeur pour la note tant que c'est un entier négatif ou positif. La moyenne sera par la suite calculer en additionnant chaque note de l'utilisateur et le divisera par le nombre de notes pour cet utilisateur
                    </p>
                  </IonText>

                  <IonText>
                    <p>
                      Par exemple, on saisit les notes 10, 20, 40 pour un même utilisateur. 
                      Puis sa moyenne sera donc <IonBadge color="light">10 + 20 + 40 =  70 / 3 = 23,33</IonBadge>
                    </p>
                  </IonText>

                  <IonText>
                    <p>
                      <b>NB:</b> Un utilisateur ne sera ajouter que si l'email saisit n'existe pas encore dans la base de donnée. Si vous entrez différentes informations pour un même email alors la note sera attribuer à l'utilisateur portant cet email. 
                    </p>
                  </IonText>
                  <IonText><h3>List user section</h3></IonText>
                  <IonText>
                    <p>
                      Cette section liste: les utilisateurs et leurs moyennes. Pour voir la liste des notes pour chaque utilisateur, cliquez sur leur noms.
                    </p>
                  </IonText>
                </IonList>

              </IonList>
          </IonCol>
          <IonCol sizeLg='6' sizeMd='8' sizeSm='12'  sizeXs='12'>
            <IonList className="app-container-page">
              <IonList className="app-container-item">
                <IonList className='app-container-navigation'>
                  <NavHeader />

                </IonList>
           
                  <IonReactRouter>
                    <Route exact path="/list-user" component={ListPage} />
                    <Route exact path="/" component={Home} />
                  </IonReactRouter>
               
              </IonList>
            </IonList>

          </IonCol>
        </IonRow>
      </IonGrid>
    </IonContent>
  </IonApp>
);

export default App;
