import { createContext } from 'react';
export const useContextApp = createContext(0);
export const useContextSearch = createContext('');

