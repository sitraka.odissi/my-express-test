import Field from "../fields/field";
import Button from "../buttons/button";
import React , {useContext, useState} from "react";
import { IonGrid, IonCol, IonRow, IonIcon } from "@ionic/react";
import { searchOutline } from "ionicons/icons";
import { useContextSearch } from "../state/context";

interface SearchProps{
  actionButton?: () =>void;
}
const Search: React.FC<SearchProps> = ({actionButton}) => {
  const [search, setSearch] = useState("");
    // const ProvSearch = useContext(useContext)
  const getInputSearch = (e: any) => {
    setSearch(e.target.value);
    console.log(search);
  };
  return (
    <useContextSearch.Provider value={search}>
      <IonGrid>
        <IonRow className="app-container-search">
          <IonCol size="12">
            <Field
              isSimple={true}
              classNameField="field-control search"
              idField="search-input"
              onChangeFunction={getInputSearch}
              placeholderField="Search email"
              valueIntDefault={search}
            />
            <Button
              classCustom="search" action={actionButton}
              content={<IonIcon icon={searchOutline}></IonIcon>}
            />
          </IonCol>
        </IonRow>
      </IonGrid>
    </useContextSearch.Provider>
  );
};

export default Search;
