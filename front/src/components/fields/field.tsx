import {IonInput , IonItem, IonLabel, IonIcon} from '@ionic/react';
import React,{ComponentProps, useState} from 'react';
import internal from 'stream';
import {alertCircle} from 'ionicons/icons';
interface FieldProps{
    typeField?: ComponentProps<typeof IonInput>['type'];
    classNameField: string;
    placeholderField?: string;
    nameField?: string;
    idField: string;
    labelField?: string;
    isSimple?: boolean;
    valueIntDefault?:number|string;
    onChangeFunction?: (e: any) => any;
    isDanger?: boolean
   
}
const Field:React.FC<FieldProps> = ({typeField, classNameField, isDanger ,placeholderField, nameField, idField, labelField, isSimple=false ,valueIntDefault, onChangeFunction}) =>{
     //Icon field=================
  const [iconField, setIconField] = useState(true);
    if(valueIntDefault && typeof valueIntDefault === "string" && valueIntDefault.length == 0){
        setIconField(false);
    }
    if(isSimple){
        return(
            <IonInput type={typeField} id={idField}  onIonInput={onChangeFunction} className={classNameField} name={nameField} value={valueIntDefault} placeholder={placeholderField}/>
        )
    }
        return(
            <IonItem>
                <IonLabel position='floating' className={`app-component-field-label ${isDanger?" danger": null}`}>{labelField}</IonLabel>
                <IonInput type={typeField} id={idField} onIonInput={onChangeFunction} value={valueIntDefault} className={`${classNameField} ${isDanger?" danger": null}`} name={nameField} placeholder={placeholderField}/>
                {isDanger ? <IonIcon className='icon-alert-field' icon={alertCircle}></IonIcon>: null}
            </IonItem>
        );
    }


export default Field;