import React from 'react';
import { Link , NavLink} from 'react-router-dom';

import { IonContent, IonMenu, IonItem , IonHeader, IonLabel, IonList, IonRouterLink, IonNavLink, IonNav} from '@ionic/react';

const NavHeader: React.FC = () => {
    
    return (
      
        <IonList className='header-navigation' >
          <IonNavLink className='header-navigation-link'  router-direction="/">
            <IonRouterLink routerLink='/'>Home</IonRouterLink>
          </IonNavLink>
          <IonNavLink className='header-navigation-link' router-direction="/list-user">
            <IonRouterLink routerLink='/list-user'>List user</IonRouterLink>
          </IonNavLink>
        </IonList>
    );
}
export { NavHeader}