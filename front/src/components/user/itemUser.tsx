import {
  IonContent,
  IonRow,
  IonCol,
  IonCardContent,
  IonGrid,
  IonIcon,
  IonText,
  IonPopover,
  IonButton,
} from "@ionic/react";
import React from "react";
import { eyeOutline, createOutline, trashOutline } from "ionicons/icons";
import Button from "../buttons/button";

type Note = { value: number };
interface UserProps {
  firstname?: string;
  lastname?: string;
  email: string;
  notesUser: Note[];
  noteAverage?: string;
  index: Number;
}
const User: React.FC<UserProps> = ({
  firstname,
  lastname,
  email,
  noteAverage,
  index,
  notesUser,
}) => {
  return (
    <IonGrid no-padding>
      <IonRow className="user-content-item" no-padding>
        <IonCol size="9" className="user-name" no-padding>
          <IonText id={`user-trigger-note-${index}`}>
            <h3 className="user-item-name">
              {firstname} {lastname}
            </h3>
          </IonText>

          <IonText>
            <small>{email}</small>
          </IonText>
          <IonPopover trigger={`user-trigger-note-${index}`}>
            <IonContent>
              <IonGrid>
                <IonRow>
                  {notesUser.map((note, index) => {
                    return (
                      <IonCol key={index} size="12">
                        <IonGrid>
                          <IonRow>
                            <IonCol size="6">
                              <IonText className="user-note-content">
                                  <p>Note {index+1}</p>
                              </IonText>
                            </IonCol>
                            <IonCol size="6">
                              <IonText className="user-note-content">
                                  <p>{note.value}</p>
                              </IonText>
                            </IonCol>
                          </IonRow>
                        </IonGrid>
                      </IonCol>
                    );
                  })}
                </IonRow>
              </IonGrid>
            </IonContent>
          </IonPopover>
        </IonCol>
        <IonCol size="3" className="user-actions">
          <IonCol className="user-action-item view" no-padding>
            <IonText>
              <h3 className="user-noteAvairage">{noteAverage}</h3>
            </IonText>
          </IonCol>
        </IonCol>
      </IonRow>
    </IonGrid>
  );
};
export default User;
