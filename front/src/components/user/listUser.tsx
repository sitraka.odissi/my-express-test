import React, { useContext, useEffect, useState } from "react";
import { IonContent, IonList, IonItem, IonCol, IonRow , IonGrid, IonIcon} from "@ionic/react";
import axios from "axios";
import User from "./itemUser";
import Search from "../search/search";
import Field from '../fields/field'
import Button from '../buttons/button'
import { searchOutline } from "ionicons/icons";

import {useContextSearch} from '../state/context';

import { type } from "os";
interface Users {
  Field?: React.ReactNode;
  Button?: React.ReactNode;
  User?: React.ReactNode;
}
type Note = {
  id: string;
  value: number;
};
type Search ={
  value: string
}

type User = {
  id: string;
  firstname: string;
  lastname: string;
  noteAverage: string;
  notes: Note[];
  email: string;
};

const Users: React.FC<Users> = () => {
  const [users, setUsers] = useState<User[]>([]);
  const [userSlice, setUserSlice ] = useState<User[]>([]);
  const [search, setSearch] = useState("");
  
  useEffect(() => {
    // Fetch users data
    axios
      .post("http://localhost:3000/graphql", {
        query: `
                query {
                    users {
                      email
                      firstname
                      lastname
                      noteAverage
                      notes {
                        value
                      }
                    }
                  }
                `,
      })
      .then((response) => {
        setUsers(response.data.data.users);
        setUserSlice(response.data.data.users);
        // console.log("Users -> ", users);
      });
  }, []);
  
  
  const handleSearch =() =>{
    // console.log('searchValue->' , search);
    
    const userFiltred = users.filter((user => {
      return user.email.toLowerCase().includes(search.toLowerCase()) ||
      user.firstname.toLowerCase().includes(search.toLowerCase()) ||
      user.lastname.toLowerCase().includes(search.toLowerCase())
    }));
    
    setUserSlice(userFiltred);
  }
  // const ProvSearch = useContext(useContext)
  const getInputSearch = (e: any) => {
    setSearch(e.target.value);
    // console.log(search);
  };
  return (
    <IonRow className='home-row-parent'>
    <IonCol size='12'>
    <IonGrid>
        <IonRow className="app-container-search">
          <IonCol size="12">
            <Field
              isSimple={true}
              classNameField="field-control search"
              idField="search-input"
              onChangeFunction={getInputSearch}
              placeholderField="Search email"
              valueIntDefault={search}
            />
            <Button
            classCustom='search'
              action={handleSearch}
              content={<IonIcon icon={searchOutline}></IonIcon>}
            />
          </IonCol>
        </IonRow>
      </IonGrid>       
    </IonCol>
    <IonCol size="12" className='home-container-user'>
    <IonList>
      {userSlice.map((user, index) => {
        return (
          <IonItem key={index}  lines="none" className="user-list-item">
            <User
              firstname={user.firstname}
              lastname={user.lastname}
              email={user.email}
              noteAverage={user.noteAverage}
              index={index}
              notesUser={user.notes}
            />
          </IonItem>
        );
      })}
    </IonList>
    </IonCol>
</IonRow>


    
  );
};
export default Users;
