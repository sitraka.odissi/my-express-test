import { IonButton , IonIcon} from '@ionic/react';
import React from 'react';
import { eyeOutline } from 'ionicons/icons'

interface ButtonProps{
    action?: () => void;
    content: any;
    classCustom?: string;
}
const Button:React.FC<ButtonProps> = ({action, content, classCustom}) =>{
    return(
        <IonButton className={`btn btn-${classCustom}`} onClick={action}>{content}</IonButton>
    );
}
export default Button;