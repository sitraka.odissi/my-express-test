import React, { FormEvent, useEffect, useState, useContext } from "react";
import { IonContent, IonList, IonIcon, useIonToast } from "@ionic/react";
import Field from "../fields/field";
import Button from "../buttons/button";
import { addOutline, logoPaypal, removeOutline , checkmarkDoneOutline, alertCircle} from "ionicons/icons";
import axios from "axios";

interface Form {
  Field?: React.ReactNode;
  Button?: React.ReactNode;
  FieldNote?: React.ReactNode;
}
const Form: React.FC = () => {
  //array users================
  const [noteItem, setNoteItem] = useState(0);
  const [isDanger, setIsDanger] = useState(false)
  //State Danger icon==========
  const [isDangerName, setIsDangerName] = useState(false)
  const [isDangerLastName, setIsDangerLastName] = useState(false)
  const [isDangerEmail, setIsDangerEmail] = useState(false)

  //field state================
  const [firstname, setFirstname] = useState("");
  const [lastname, setLastname] = useState("");
  const [email, setEmail] = useState("");
 
  //Message request:
  const [showToastMess, setToastMess] = useState(false);
  const [present, dismiss] = useIonToast();
  
  const handleAddUser = (): void => {
    if(firstname.length == 0){
      setIsDangerName(true)
    }
 
   
    if(lastname.length == 0){
      setIsDangerLastName(true)
    }else{
      setIsDangerLastName(false)
    }
    if(email.length == 0){
      setIsDangerEmail(true)
    }else{
      setIsDangerEmail(false)
    }

    if (firstname.length !== 0 && lastname.length !== 0) {
       
      axios
        .post("http://localhost:3000/graphql", {
          query: `
              mutation{
                  createUser(
                      input: {
                          firstname:"${firstname}",
                          lastname:"${lastname}",
                          email:"${email}"
                          notes:[
                            { value:${noteUser} },
                          ]
                      }
                  ) {
                   user {
                    firstname
                    id
                    email
                    notes {
                      id
                      value
                    }
                    noteAverage
                    lastname
                   },
                   isExisting
                  }
                }
              `,
        })
        .then((response) =>{
          console.log("POST response", response);
          setToastMess(true)
          setEmail("");
          setFirstname("");
          setLastname("")
          setNoteUser(0)
        });
        
    } else {
      setIsDanger(true);
    }
  };
  if(showToastMess){
    present({
      buttons: [{ text: 'close', handler: () => dismiss() }],
      message: 'Added successfuly',
      icon: checkmarkDoneOutline,
      cssClass: 'toast-success',
      onDidDismiss: () => setIsDanger(false),
      onWillDismiss: () => setIsDanger(false),
    })
  }
  const getFirstname = (e: any) => {
    if(isDangerName){
      setIsDangerName(false)
    } 
    setFirstname(e.target.value);
  };
  const getLastname = (e: any) => {
    if(isDangerLastName){
      setIsDangerLastName(false)
    }
    setLastname(e.target.value);
  };
  const getEmail = (e: any) => {
    if(isDangerEmail){
      setIsDangerEmail(false)
    }
    setEmail(e.target.value);
  };

  const [noteUser, setNoteUser] = useState<number>(0);
  const addNote = () => {
    const noteInt: number = +noteUser;
    setNoteUser(noteInt + 1)
  }
  const handleChangeNote = (e: any): void => {
    const noteEvent = e.target.value;
    setNoteUser(noteEvent);

  }
  const removeNote = () => {
    const noteInt: number = +noteUser;
    setNoteUser(noteInt - 1)
  }


  return (
    <form action="">
      <Field
        typeField="text"
        labelField="Name"
        idField="name"
        classNameField='field-control'
        placeholderField="Name"
        onChangeFunction={getFirstname}
        valueIntDefault={firstname}
        isDanger={isDangerName}
      />
      <Field
        typeField="text"
        labelField="Last Name"
        idField="lastname"
        classNameField='field-control'
        placeholderField="Last Name"
        onChangeFunction={getLastname}
        valueIntDefault={lastname}
        isDanger={isDangerLastName}

      />
      <Field
        typeField="email"
        labelField="Email"
        idField="mail"
        classNameField='field-control'
        placeholderField="Email"
        onChangeFunction={getEmail}
        valueIntDefault={email}
        isDanger={isDangerEmail}

       
      />
      <IonList className='field-note-user'>
        <Field isSimple={true} typeField="number" onChangeFunction={handleChangeNote} idField="note" valueIntDefault={noteUser} classNameField='field-control input-note' />
        <IonList className='button-for-number'>
          <Button action={addNote} classCustom="add-note" content={<IonIcon icon={addOutline}></IonIcon>} />
          <Button action={removeNote} classCustom="remove-note" content={<IonIcon icon={removeOutline}></IonIcon>} />
        </IonList>
      </IonList>

      <Button
        action={handleAddUser}
        classCustom="add-user"
        content="Add User"
      />
    </form>
  );
};
export default Form;
