import React, { FormEvent, useEffect, useState } from 'react';
import { IonContent, IonList, IonIcon } from '@ionic/react';
import Field from '../fields/field';
import Button from '../buttons/button';
import { addOutline , logoPaypal, removeOutline} from 'ionicons/icons';
import {useContextApp} from '../state/context';


const FieldNote:React.FC<any> = (getNoteItem: (note: number) => void) =>{

    const [note, setNote] = useState<number>(0);
    const addNote = () =>{
        const noteInt : number = +note;
     setNote(noteInt + 1)
    }
    const handleChangeNote = (e: any): void => {
        const noteEvent = e.target.value >= 20 ? 20 : e.target.value;
        setNote(noteEvent);
        console.log(note);
        getNoteItem(note);
    }
    const removeNote = () =>{
        const noteInt : number = +note;
        setNote(noteInt - 1)
    }    
    return(
        <IonList className='field-note-user'>
                <Field isSimple={true} typeField="number" onChangeFunction={() => handleChangeNote} idField="note" valueIntDefault={note} classNameField='field-control input-note' />
                <IonList className='button-for-number'>
                    <Button action={addNote} classCustom="add-note" content={<IonIcon icon={addOutline}></IonIcon>}/>
                    <Button action={removeNote} classCustom="remove-note" content={<IonIcon icon={removeOutline}></IonIcon>}/>
                </IonList>      
        </IonList>
    );
}
export default FieldNote;