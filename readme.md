# Installation

- [Avec Docker](#méthode-1-avec-docker)
- [Sans Docker](#méthode-2-sans-docker)

## **Méthode 1: Avec Docker**

> Pour l'installer avec Docker, il faut télécharger une version compatible de son système d'exploitation de `docker-compose` et `docker`

Puis éxcécuter la commande, dans le dossier racine du projet

```
docker-compose up --build
```

Puis pour lancer la migration:

```
docker-compose exec server npm run db:migrate
```

## **Méthode 2: Sans Docker**

> Pré-requis: Pour que l'application fonctionne, il faut installer au préalable
>
> - Postgres
> - Node v16+

#### Installation du Backend

Créer la base de données dans Postgres portant le nom `app`

Avant de commencer, copiez le fichier `.env.example` du dossier **back/** dans le même dossier; puis renommez-le en `.env`

Pour installer la partie Backend du projet, il faut exécuter la
commande suivante en partant de la racine du dossier **back/**

```
npm install
```

Pour lancer la migration

```
npm run db:migrate
```

Pour lancer le projet en mode **DEVELOPPEMENT**, on execute la commande

```
npm run dev
```

Pour lancer le projet en mode **PRODUCTION**, on execute la commande

```
npm run build && npm run start
```

> Si l'on souhaite voir et tester les APIs avec une interface,

Pour accéder à l'API, accéder à l'url `http://localhost:3000/graphql`

---

#### Installation du Frontend

Télécharger `@ionic-cli` en éxecutant la commande:

```
npm install -g @ionic/cli
```

Pour lancer le mode **DEVELOPPEMENT** du projet, exécutez la commande suivante dans le dossier **front/** :

```
ionic serve -p 8000
```

Pour lancer le mode **PRODUCTION** du projet, exécutez la commande suivante dans le dossier **front/** :

```
npm build && npm start
```

Accéder à l'url `http://loclahost:8000` pour voir le résultat
