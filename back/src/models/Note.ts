import { Model, DataTypes, DATE } from "sequelize";
import { sequelize } from "../database/postgres";

export default class Note extends Model {}

Note.init(
  {
    id: {
      type: DataTypes.UUIDV4,
      primaryKey: true,
    },
    value: DataTypes.INTEGER,
    userId: DataTypes.UUIDV4,
  },
  { sequelize, modelName: "note" }
);
