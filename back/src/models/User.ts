import { Model, DataTypes } from "sequelize";
import { sequelize } from "../database/postgres";

export default class User extends Model {}

User.init(
  {
    id: {
      type: DataTypes.UUIDV4,
      primaryKey: true,
    },
    firstname: DataTypes.STRING,
    lastname: DataTypes.STRING,
    email: {
      type: DataTypes.STRING,
      unique: true,
    },
  },
  { sequelize, modelName: "user" }
);
