import { gql } from "apollo-server-express";

export const typeDefs = gql`
  type User {
    id: ID!
    firstname: String
    lastname: String
    email: String
    notes: [Note]
    noteAverage: Float
  }

  type Note {
    id: ID!
    value: Int
  }

  input NotesInput {
    value: Int
  }

  input UserInput {
    firstname: String
    lastname: String
    email: String
    notes: [NotesInput]
  }

  type createUserResult {
    isExisting: Boolean
    user: User
  }

  type Mutation {
    createUser(input: UserInput): createUserResult
  }

  type Query {
    users: [User]
    notes: [Note]
    user(email: String): User
  }
`;
