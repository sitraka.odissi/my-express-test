import { v4 as uuid } from "uuid";

import { User, Note } from "../models";
import { UserContextType } from "../types/context.type";
import { sequelize } from "../database/postgres";

import validator from "validator";

export const resolvers = {
  Mutation: {
    createUser: async (parent: unknown, context: UserContextType) => {
      const userId = uuid();

      const { firstname, lastname, email, notes } = JSON.parse(
        JSON.stringify(context.input)
      );

      const foundUser: any = await User.findOne({
        where: { email },
      });

      const createdNotes = notes.map((note: { value: number }) => {
        const noteId = uuid();
        const { value } = note;

        return {
          id: noteId,
          value,
          userId: foundUser ? foundUser.id : userId,
        };
      });

      const noteQuery = await Note.bulkCreate(createdNotes);

      if (!foundUser) {
        const userQuery = await User.create({
          id: userId,
          firstname,
          lastname,
          email,
        });

        return {
          isExisting: false,
          user: {
            id: userQuery.getDataValue("id"),
            firstname: userQuery.getDataValue("firstname"),
            lastname: userQuery.getDataValue("lastname"),
            email: userQuery.getDataValue("email"),
          }
        };
      } else {
        return {
          isExisting: true,
          user: {
            id: foundUser.id,
            firstname: foundUser.firstname,
            lastname: foundUser.lastname,
            email: foundUser.email,
          }
        };
      }
    },
  },

  Query: {
    users: async () => {
      const users = await User.findAll();
      return users;
    },
    notes: async () => {
      const notes = await Note.findAll();
      return notes;
    },
    user: async (
      obj: any,
      args: { email: string },
      context: any,
      info: any
    ) => {
      const user = await User.findOne({
        where: {
          email: args.email,
        },
      });

      return user;
    },
  },

  User: {
    notes: async (user: { id: string }) => {
      const notes = await Note.findAll({
        where: {
          userId: user.id,
        },
      });

      return notes;
    },
    noteAverage: async (user: { id: string }) => {
      const note: any = await Note.findOne({
        where: {
          userId: user.id,
        },
        attributes: [
          [sequelize.fn("AVG", sequelize.col("value")), "noteAverage"],
        ],
        group: "userId",
        order: [[sequelize.fn("AVG", sequelize.col("value")), "DESC"]],
      });

      return parseFloat(note.getDataValue("noteAverage")).toFixed(2);
    },
  },
};
