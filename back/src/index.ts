import express, { Express, Request, Response } from "express";
import dotenv from "dotenv";
import { ApolloServer } from "apollo-server-express";

import { resolvers, typeDefs } from "./graphql";
import * as postgresDb from "./database/postgres";

dotenv.config();

const app: Express = express();
const port = process.env.APP_PORT;

// Create the graphql server
let server = null;
const startServer = async () => {
  server = new ApolloServer({ typeDefs, resolvers });
  await server.start();
  server.applyMiddleware({
    app,
    path: "/graphql",
  });
};

// Start the graphql server
startServer();

// Connect to the Postgres DB
postgresDb.connect();

app.get("/", (req: Request, res: Response) => {
  return res.send("Express and Typescript are running together");
});

app.listen(port, () => {
  console.log(`[server]: Server is running at http://localhost:${port}`);
});
