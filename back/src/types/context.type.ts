export type UserContextType = {
  input: {
    firstname: string;
    lastname: string;
    email: string;
    notes: NoteContextType[];
  };
};

export type NoteContextType = {
  value: number;
};
