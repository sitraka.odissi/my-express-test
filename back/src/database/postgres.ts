import { Sequelize } from "sequelize";
import dotenv from "dotenv";

dotenv.config();

const {
  POSTGRES_HOST,
  POSTGRES_USER,
  POSTGRES_PASSWORD,
  POSTGRES_NAME,
  POSTGRES_PORT,
} = process.env;

// console.debug(process.env);

const sequelize = new Sequelize(
  `postgres://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${POSTGRES_HOST}:${POSTGRES_PORT}/${POSTGRES_NAME}`
);

// console.debug(sequelize);

const connect = () => {
  try {
    sequelize
      .authenticate()
      .then(() => console.log("Postgres connection established"));
  } catch (error) {
    console.error("Unable to connect with Postgres ", error);
  }
};

export { sequelize, connect };
