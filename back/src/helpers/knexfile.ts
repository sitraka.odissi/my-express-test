// Make migration for Postgres Database

import { Knex } from "knex";

import dotenv from "dotenv";
import path from "path";

dotenv.config({ path: path.resolve("../..") + "/.env" });
const {
  POSTGRES_HOST,
  POSTGRES_USER,
  POSTGRES_PASSWORD,
  POSTGRES_NAME,
  POSTGRES_PORT,
} = process.env;

const config = {
  client: "pg",
  connection: {
    database: POSTGRES_NAME,
    user: POSTGRES_USER,
    password: POSTGRES_PASSWORD,
    host: POSTGRES_HOST,
    port: POSTGRES_PORT,
  },
  migrations: {
    directory: "./migrations",
  },
};

export default config as Knex.Config;
