import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable("notes", (table) => {
    table.uuid("id").primary();
    table.integer("value").notNullable();
    table.uuid("userId").notNullable();
    table.timestamps(true, undefined, true);
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable("notes");
}
